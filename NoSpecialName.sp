/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NoSpecialName.sp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: DragonSoul <dragonsoul@ovh.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 04:36:28 by DragonSoul        #+#    #+#             */
/*   Updated: 2015/11/22 09:23:55 by DragonSoul       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sourcemod>
#include <sdktools>

#pragma newdecls required

#define MAXCHARS		500
#define CHAR_MAXLEN		5
#define NAME_LEN		64
#define	NSN_IGNORE_BOT	1
#define	NAME_KICK_MSG	"Incorrect nickname. Please read server rules (www.cscargo.net)"

/* ************************************************************************** */
/*                                    NOTE                                    */
/*                                                                            */
/* - Forward permettant d'avoir un event apres le changement d'un pseudo      */
/* - Possibilite d'ajouter un filtre sur les mots                             */
/* - Possibilite de creer des regles de substitution de caracteres            */
/*                                                                            */
/* ************************************************************************** */

char	g_config[NAME_LEN] = "/addons/sourcemod/configs/authorized.cfg";
char	g_authorised[MAXCHARS] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ -";
int		g_ok_chars = 54;				/* update it when adding authorised chars */
Handle	g_NicknameChange;

forward void	OnNicknameChange(int client);

public Plugin	myinfo =
{
	name = "NoSpecialName",
	description = "Remove special characters in nickname.",
	author = "DragonSoul",
	version = "0.3",
	url = "http://www.cscargo.net"
};

/*
** cr�ation du forward et des commandes
*/
public void		OnPluginStart()
{
	g_NicknameChange = CreateGlobalForward("OnNicknameChange", ET_Event, Param_Cell);
	HookEvent("player_spawn", Event_PlayerSpawn);
	// changer tag selon besoin
	RegAdminCmd("sm_reloadchar", ReloadConfig, ADMFLAG_CONFIG, "Recharger le fichier de config des caractere autoris�s");
}

public void		OnMapStart()
{
	InitiateConfig();
}

/*
** fonction sm_reloadchar
*/
public Action	ReloadConfig(int client, int args)
{	
	InitiateConfig();
	LogAction(client, -1, "NoSpecialName: Reloaded config file");
	ReplyToCommand(client, "NoSpecialName: Reloaded config file");
	return (Plugin_Handled);
}

/*
** Initialise la configuration du filtre de caract�res
*/
stock void		InitiateConfig()
{
	int		i = -1;
	Handle	authorised_char = OpenFile(g_config, "r");

	if (authorised_char != INVALID_HANDLE)
	{
		while ((++i < MAXCHARS) && ReadFileLine(authorised_char, g_authorised[i], CHAR_MAXLEN)) // bug possible
			TrimString(g_authorised[i]);
		PrintToServer("NoSpecialName: Nombre de caract�res authoris�s : %d ",i); // compte aussi les lignes vides
		g_ok_chars = i;
		CloseHandle(authorised_char);
	}
}

/*
** V�rifie que le caract�re soit autoris�.
*/
bool			is_correct_char(char c)
{
	for(int i = 0; i < g_ok_chars; ++i)
	{
		if (g_authorised[i] == c)
			return (true);
	}
	return (false);
}

/*
** Renvoi le caract�re de remplacement
** a coder
*/
char			get_correct_char(char c)
{
	if (c)
		return (view_as<char>'.');
	else
		return (view_as<char>'.');
}

/*
** Test si le pseudo est correct de facon optimisee
*/
bool			is_nickname_correct(char[] name)
{
	int		i = -1;
	bool	vowel = false;
	bool	con = false;

	while (name[++i])
	{
		if (!is_correct_char(name[i]))
			return (false);
		else
		{
			vowel |= is_vowel(name[i]);
			con |= (is_alpha(name[i]) && !is_vowel(name[i]));
		}
	}
	return (i >= 3);
}

/*
** modifie le pseudo selon les r�gles
*/
void			correct_nickname(char nickname[NAME_LEN])
{
	int		i = 0;
	int		j = 0;
	char	c;

	while (nickname[i + j] && (i + j) < NAME_LEN - 1)
	{
		if (!is_correct_char(nickname[i + j]))
		{
			c = get_correct_char(nickname[i + j]);
			if (c != '.')
				nickname[i++] = c;
			else
				++j;
		}
		else
			nickname[i] = nickname[i++ + j];
	}
	nickname[i] = '\0';
	TrimString(nickname);
}

/*
** cree un forward "OnNicknameChange"
** permet la programmation evenementielle
*/
void			Call_nick_handler(int client)
{
	Action	result;

	Call_StartForward(g_NicknameChange);
	Call_PushCell(client);
	Call_Finish(view_as<int>result);
}

/*
** retourne si le nombre est un caractere alphabetique
** boolean value
*/
bool			is_alpha(char c)
{
	if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
		return (true);
	else
		return (false);
}

/*
** retourne si le nombre est une voyelle
** boolean value
*/
bool			is_vowel(char c)
{
	if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y'
		|| c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U' || c == 'Y')
		return (true);
	else
		return (false);
}

/*
** Compte le nombre de voyelles
*/
int				count_vowel(char[] name)
{
	int	result = 0;
	int	i = -1;

	while (name[++i])
		result += view_as<int>is_vowel(name[i]);
	PrintToServer("Voyelles: %d", result);
	return (result);
}

/*
** Compte le nombre de consonnes
*/
int				count_consonant(char[] name)
{
	int	result = 0;
	int	i = -1;

	while (name[++i])
		result += view_as<int>(is_alpha(name[i]) && !is_vowel(name[i]));
	PrintToServer("Consonnes: %d", result);
	return (result);
}

/*
** detecteur de mots interdits
** a coder
*/
bool			is_punk_word(char[] name)
{
	strlen(name); // pass warning
	return (false);
}

/*
** Verifie si le pseudo est toujours hors charte
*/
int				is_insane_nickname(char[] name)
{
	if (strlen(name) < 3 || !count_vowel(name) || !count_consonant(name))
		return (1);
	else if (is_punk_word(name)) // ajouter detecteur mots vulgaires
		return (1);
	else
		return (0);
}

/*
** Verifie automatiquement le pseudo d'un joueur
*/
void			NoSpecialName(int client)
{
	char	name[NAME_LEN] = "";
	char	cname[NAME_LEN];
 
	if (IsFakeClient(client) && NSN_IGNORE_BOT)
		return ;
	GetClientName(client, name, NAME_LEN - 1);
	strcopy(cname, NAME_LEN, name);
	if (!is_nickname_correct(name))
	{
		correct_nickname(name);
		if (is_insane_nickname(name))
			KickClient(client, NAME_KICK_MSG);
		else
		{
			SetClientName(client,name);
			PrintToChat(client, "Your nickname was changed to match server's rules.");
			Call_nick_handler(client);
		}
	}
}

/*
** Event de connection
*/
public void		OnClientPutInServer(int client)
{
	NoSpecialName(client);
}

/*
** Event de spawn
*/
public void		Event_PlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	NoSpecialName(GetClientOfUserId(GetEventInt(event, "userid")));
} 
